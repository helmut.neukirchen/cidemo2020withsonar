# Changelog

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.1] - 2020-02-20
### Fixed
- NPE when opening huge files (Issue #6)

## [1.0.0] - 2020-02-20
### Added
- Add Save as (Issue #5)
- Add Print (Issue #4)

### Changed
- Close should ask to save changes (Issue #3)

### Removed
- Remove support for API version 0.1 (Issue #2)

## [0.9.0] - 2020-01-03
