# CI demo

[![pipeline status](https://gitlab.com/helmut.neukirchen/cidemo2020withsonar/badges/master/pipeline.svg)](https://gitlab.com/helmut.neukirchen/cidemo2020withsonar/-/commits/master)

[![coverage report](https://gitlab.com/helmut.neukirchen/cidemo2020withsonar/badges/master/coverage.svg)](https://gitlab.com/helmut.neukirchen/cidemo2020withsonar/-/commits/master)

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=helmut.neukirchen_cidemo2020withsonar&metric=alert_status)](https://sonarcloud.io/dashboard?id=helmut.neukirchen_cidemo2020withsonar)

[Site Report](https://helmut.neukirchen.gitlab.io/cidemo2020withsonar/), e.g. test coverage