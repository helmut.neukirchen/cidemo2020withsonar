package is.hi.cs.junit4demo;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ MoneyTestWithFixture.class })
public class AllTests {
	  // the class remains completely empty,
	  // being used only as a holder for the above annotations
}